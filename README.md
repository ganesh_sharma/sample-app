# sample-app

A project to demonstrate:

- Building a 'Hello World' Application

Manually Containerising  Application
- Containerising Application using Docker Desktop
- Manually Pushing app Image to Gitlab Container Registry

Creation of Azure Kubernetes Service (AKS) & Connection to Gitlab CI/CD
- Creating an AKS Cluster Using Azure Portal or Azure CLI or Terraform
- Connecting Gitlab to AKS

Manually Deploying an Application on AKS
- Manually Deploying Application to AKS

Automated build & deployment using Gitlab CI/CD
- Using Gitlab CI/CD to build an Image
- Using Gitlab CI/CD to deploy an Image