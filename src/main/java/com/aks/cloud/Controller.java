package com.aks.cloud;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping(path="/status")
    public String getHome() {
        return "Hello World. I've just deployed app onto AKS by Gitlab.";
    }

}
